#include  <stdio.h>
#include  <stdlib.h>
#include  <sys/socket.h>
#include  <netdb.h>
#include  <string.h>
#include  <unistd.h>
#include <stdbool.h>
#include <setjmp.h>
#include <sys/types.h>

#define SERVADDR "localhost" // Définition de l'adresse IP d'écoute
#define SERVPORT "12345" // Définition du port d'écoute

#define LISTENLEN 1  //Pour l'instant, on s'occupe uniquement d'un seul client
#define MAXBUFFERLEN 1024 //
#define MAXHOSTLEN 64   //
#define MAXPORTLEN 6    //
#define PORTFTP "21"    //

int main() {
        int ecode; // Code erreur pour retour des fonctions read et write
        char serverAddr[MAXHOSTLEN]; // Adresse du serveur dans le proxy
        char serverPort[MAXPORTLEN]; // Port du server dans le proxy

        int descSockRDV; // Descriptor socket de rendez-vous
        int connexionClientCtrl; // Descriptor socket de communication
        int connexionServeur; // Descriptor socket de communication
        int dataClient; //Descriptor socket data client
        int dataServeur; //Descriptor socket data serveur

        struct addrinfo hintsClient; // Contrôle la fonction getaddrinfo POUR LE CLIENT
        struct addrinfo hintsServeur; // Idem as up, mais POUR LE SERVUER
        struct addrinfo hintsDataClient; //Contrôle la fonction getaddrinfo pour le client DONNÉES
        struct addrinfo hintsDataServeur; //Contrôle la fonction getaddrinfo pour le serveur DONNÉES

        struct addrinfo * res; // Contient le résultat de la fonction getaddrinfo POUR LE CLIENT
        struct addrinfo * resServeur, * resServeurPTR; // Contient le résultat de la fonction getaddrinfo POUR LE SERVEUR
        struct addrinfo * resDataServeur, * resDataClient, * resDataClientPTR, * resDataServeurPTR;

        struct sockaddr_storage infordv; // Informations sur la socket de RDV
        struct sockaddr_storage infoclient; // Informations sur le client connecté

        socklen_t len; // Variable utilisée pour stocker les 
        // longueurs des structures de socket
        char buffer[MAXBUFFERLEN]; // Tampon de communication entre le client et le serveur

        // Publication de la socket pour le système
        // Port et IP assignés
        // Vidage emplacement mémoire pour hintsClient
        memset( & hintsClient, 0, sizeof(hintsClient));
        // Initailisation de hintsClient
        hintsClient.ai_flags = AI_PASSIVE; // mode serveur, nous allons utiliser la fonction bind
        hintsClient.ai_socktype = SOCK_STREAM; // Correspond à l'utilisation de TCP
        hintsClient.ai_family = AF_INET; // INET = IPV4

        memset( & hintsServeur, 0, sizeof(hintsServeur));
        hintsServeur.ai_socktype = SOCK_STREAM;
        hintsServeur.ai_family = AF_INET;

        // Récupération des informations sur le serveur
        ecode = getaddrinfo(SERVADDR, SERVPORT, & hintsClient, & res);
        if (ecode) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ecode));
                exit(1);
        }

        //Création de la socket de rdv
        descSockRDV = socket(res -> ai_family, res -> ai_socktype, res -> ai_protocol);
        if (descSockRDV == -1) {
                perror("Erreur creation socket");
                exit(4);
        }

        // Publication de la socket au niveau du système
        ecode = bind(descSockRDV, res -> ai_addr, res -> ai_addrlen);
        if (ecode == -1) {
                perror("Erreur liaison de la socket de RDV");
                exit(3);
        }

        freeaddrinfo(res);

        len = sizeof(struct sockaddr_storage);
        ecode = getsockname(descSockRDV, (struct sockaddr * ) & infordv, & len);
        if (ecode == -1) {
                perror("SERVEUR: getsockname");
                exit(4);
        }
        ecode = getnameinfo((struct sockaddr * ) & infordv, sizeof(infordv), serverAddr, MAXHOSTLEN,
                serverPort, MAXPORTLEN, NI_NUMERICHOST | NI_NUMERICSERV);
        if (ecode != 0) {
                fprintf(stderr, "error in getnameinfo: %s\n", gai_strerror(ecode));
                exit(4);
        }
        printf("L'adresse du proxy est: %s\n", serverAddr); //Pour s'assurer des données nécessaires pour faire se connecter
        printf("Le port du proxy est: %s\n", serverPort); //le client Ftp
        printf("PID : %d \n", getpid());
        
        ecode = listen(descSockRDV, LISTENLEN);
        if (ecode == -1) {
                perror("Erreur initialisation buffer d'écoute");
                exit(5);
        }

        len = sizeof(struct sockaddr_storage);
        // Attente connexion du client
        // Lorsque demande de connexion, creation d'une socket de communication avec le client
        connexionClientCtrl = accept(descSockRDV, (struct sockaddr * ) & infoclient, & len);
        if (connexionClientCtrl == -1) {
                perror("Erreur accept\n");
                exit(6);
        }
        // On confirme au client qu'on est bien connecté
        strcpy(buffer, "220 Proxy FTP Ready\n");

        write(connexionClientCtrl, buffer, strlen(buffer));

        //lecture USER login@serveur
        ecode = read(connexionClientCtrl, buffer, MAXBUFFERLEN);
        if (ecode == -1) {
                perror("probleme de lecture\n");
                exit(3);
        }
        buffer[ecode] = '\0';
        printf("USER_PASSWORD_IS : \"%s\"\n", buffer);

        //decoupage chaine user anonymous@adresse_serveur en v1 = anonymous et v2 = adresse_serveur
        char login[50 + 1];
        char serveurFTP[50 + 1];

        sscanf(buffer, "%50[^@]@%50s", login, serveurFTP); // qui n'est pas ^  et %s, chaîne

        strcat(login, "\n");

        //printf("Valeur de login :%s\n", login);
        //printf("Valeur de serveurFTP :%s\n", serveurFTP);

        ecode = getaddrinfo(serveurFTP, PORTFTP, & hintsServeur, & resServeur);
        if (ecode) {
                fprintf(stderr, "getaddrinfo Servor: %s\n", gai_strerror(ecode));
                exit(1);
        }

        resServeurPTR = resServeur;

        bool isConnected = false;
        while (!isConnected && resServeurPTR != NULL) {
                connexionServeur = socket(resServeurPTR -> ai_family, resServeurPTR -> ai_socktype,
                        resServeurPTR -> ai_protocol);
                if (connexionServeur == -1) {
                        perror("Erreur creation socket");
                        exit(2);
                }

                ecode = connect(connexionServeur, resServeurPTR -> ai_addr, resServeurPTR -> ai_addrlen);
                if (ecode == -1) {
                        resServeurPTR = resServeurPTR -> ai_next;
                        close(connexionServeur);
                } else
                        isConnected = true;
        }
        freeaddrinfo(resServeur);
        if (!isConnected) {
                perror("Connexion impossible");
                exit(2);
        }

        bzero(buffer, MAXBUFFERLEN - 1);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1);
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n");
                exit(3);
        }

        buffer[ecode] = '\0';
        //printf("LU SUR LE SERVEUR\"%s\".\n", buffer);

        char userLogin[50 + 1] = "USER ";
        bzero(userLogin, 50);
        strncat(userLogin, login, strlen(login));
        char retourchariot[3] = "\n";
        strncat(userLogin, retourchariot, strlen(retourchariot));
        //strncpy(buffer, "USER %s\n",login);
        //printf("ECRIT SUR LE SERVEUR %s\n", userLogin);
        ecode = write(connexionServeur, userLogin, strlen(userLogin));
        if (ecode == -1) {
                perror("probleme d'ecriture sur le serveur\n");
                exit(3);
        }

        bzero(buffer, MAXBUFFERLEN);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1);
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n");
                exit(3);
        }
        buffer[ecode] = '\0';
        //printf("LU SUR LE SERVEUR\"%s\".\n", buffer); // 331

        ecode = write(connexionClientCtrl, buffer, strlen(buffer));
        if (ecode == -1) {
                perror("probleme d'ecriture sur le client\n");
                exit(3);
        }
        bzero(buffer, MAXBUFFERLEN);

        //sleep(5);

        ecode = read(connexionClientCtrl, buffer, MAXBUFFERLEN - 1);
        if (ecode == -1) {
                perror("probleme de lecture sur le client");
                exit(3);
        }
        buffer[ecode] = '\0';
        //printf("LU SUR LE clint\"%s\".\n", buffer); // PASS XXX

        ecode = write(connexionServeur, buffer, strlen(buffer));
        if (ecode == -1) {
                perror("probleme d'ecriture sur le serveur\n");
                exit(3);
        }

        bzero(buffer, MAXBUFFERLEN);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1); //230
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n");
                exit(3);
        }
        buffer[ecode] = '\0';
       // printf("LU SUR LE SERVEUR\"%s\".\n", buffer);

        ecode = write(connexionClientCtrl, buffer, strlen(buffer));
        if (ecode == -1) {
                perror("probleme d'ecriture sur le client\n");
                exit(3);
        }
        bzero(buffer, MAXBUFFERLEN);

        ecode = read(connexionClientCtrl, buffer, MAXBUFFERLEN - 1); //SYST
        if (ecode == -1) {
                perror("probleme de lecture sur le client");
                exit(3);
        }

        ecode = write(connexionServeur, buffer, strlen(buffer)); //SYST ENVOI
        if (ecode == -1) {
                perror("probleme d'ecriture sur le serveur\n");
                exit(3);
        }

        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1); // 215
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n");
                exit(3);
        }
        buffer[ecode] = '\0';

        ecode = write(connexionClientCtrl, buffer, strlen(buffer));
        if (ecode == -1) {
                perror("probleme d'ecriture sur le client\n");
                exit(3);
        }
        bzero(buffer, MAXBUFFERLEN);

        ecode = read(connexionClientCtrl, buffer, MAXBUFFERLEN - 1); //reception PORT client
        if (ecode == -1) {
                perror("probleme de lecture sur le client");
                exit(3);
        }
       // printf("LU SUR LE CLIENT1\"%s\".\n", buffer);

        //Traduction du PORT en adresse IP pour la socket dataclient
        //et calculer le numéro de port dataclient

        int n1, n2, n3, n4, n5, n6;
        sscanf(buffer, "PORT %d,%d,%d,%d,%d,%d", &n1, &n2, &n3, &n4, &n5, &n6);
        char dataAdresseClient[25];
        char dataPortClient[10];
        sprintf(dataAdresseClient, "%d.%d.%d.%d", n1, n2, n3, n4);
        sprintf(dataPortClient, "%d", n5 * 256 + n6);
        //printf("AdresseClient:%s;PortCleint:%s\n", dataAdresseClient, dataPortClient);
        //Ouvrir dataclient
        memset( & hintsDataClient, 0, sizeof(hintsDataClient));
        hintsDataClient.ai_socktype = SOCK_STREAM;
        hintsDataClient.ai_family = AF_INET;

        ecode = getaddrinfo(dataAdresseClient, dataPortClient, & hintsDataClient, & resDataClient);
        if (ecode == -1) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ecode));
                exit(1);
        }

        resDataClientPTR = resDataClient;

        isConnected = false;
        while (!isConnected && resDataClientPTR != NULL) {
                dataClient = socket(resDataClientPTR -> ai_family, resDataClientPTR -> ai_socktype,
                        resDataClientPTR -> ai_protocol);
                if (connexionServeur == -1) {
                        perror("Erreur creation socket");
                        exit(2);
                }

                ecode = connect(dataClient, resDataClientPTR -> ai_addr, resDataClientPTR -> ai_addrlen);
                if (ecode == -1) {
                        resDataClientPTR = resDataClientPTR -> ai_next;
                        close(dataClient);
                } else
                        isConnected = true;
        }
        freeaddrinfo(resDataClientPTR);
        if (!isConnected) {
                perror("Connexion impossible");
                exit(2);
        }

        char cmdPASSV[15] = "PASV\n";

        bzero(buffer, MAXBUFFERLEN);
        strncat(buffer, cmdPASSV, strlen(cmdPASSV));

        ecode = write(connexionServeur, buffer, strlen(buffer)); //PASV au vrai serveur pour passage en mode passif
        if (ecode == -1) {
                perror("probleme d'ecriture sur le serveur\n");
                exit(3);
        }
        //printf("ECRIT SUR LE SERVEUR\"%s\".\n", buffer);
        bzero(buffer, MAXBUFFERLEN);

        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1); // 227 du serveur Entering PASS Mode avec PORT dedans
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n");
                exit(3);
        }
        //printf("LA : %s\n", buffer);

        //Pareil que pour client mais ouvrir dataserveur
        n1, n2, n3, n4, n5, n6 = 0;
        sscanf(buffer, "%*[^(](%d,%d,%d,%d,%d,%d", &n1, &n2, &n3, &n4, &n5, &n6);
        char dataAdresseServeur[25];
        char dataPortServeur[10];
        sprintf(dataAdresseServeur, "%d.%d.%d.%d", n1, n2, n3, n4);
        sprintf(dataPortServeur, "%d", n5 * 256 + n6);
       // printf("Adresse:%s;Port:%s\n", dataAdresseServeur, dataPortServeur);

        memset( & hintsDataServeur, 0, sizeof(hintsDataServeur));
        hintsDataServeur.ai_socktype = SOCK_STREAM;
        hintsDataServeur.ai_family = AF_INET;

        ecode = getaddrinfo(dataAdresseServeur, dataPortServeur, & hintsDataServeur, & resDataServeur);
        if (ecode == -1) {
                fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ecode));
                exit(1);
        }

        resDataServeurPTR = resDataServeur;

        isConnected = false;
        while (!isConnected && resDataServeurPTR != NULL) {
                dataServeur = socket(resDataServeurPTR -> ai_family, resDataServeurPTR -> ai_socktype,
                        resDataServeurPTR -> ai_protocol);
                if (connexionServeur == -1) {
                        perror("Erreur creation socket");
                        exit(2);
                }

                ecode = connect(dataServeur, resDataServeurPTR -> ai_addr, resDataServeurPTR -> ai_addrlen);
                if (ecode == -1) {
                        resDataServeurPTR = resDataServeurPTR -> ai_next;
                        close(dataServeur);
                } else
                        isConnected = true;
        }
        freeaddrinfo(resDataServeurPTR);
        if (!isConnected) {
                perror("Connexion impossible");
                exit(2);
        }

        bzero(buffer, MAXBUFFERLEN - 1);

        char commande200[30] = "200 PORT command successful\n";
        strncat(buffer, commande200, strlen(commande200));

        ecode = write(connexionClientCtrl, buffer, strlen(buffer)); //Envoyer la réponse au client 200 PORT COMMAND SUCCESFUL
        if (ecode == -1) {
                perror("probleme d'ecriture sur le client\n");
                exit(3);
        }
        //printf("LA2 : %s\n", buffer);
        bzero(buffer, MAXBUFFERLEN);

        ecode = read(connexionClientCtrl, buffer, MAXBUFFERLEN - 1); //LIST recu du client
        if (ecode == -1) {
                perror("probleme de lecture sur le client");
                exit(3);
        }
        //printf("LA3 : %s\n", buffer);
        ecode = write(connexionServeur, buffer, strlen(buffer)); //Tranfert au serveur
        if (ecode == -1) {
                perror("probleme d'ecriture sur le serveur\n");
                exit(3);
        }
        //printf("LU SUR LE SERVEUR\"%s\".\n", buffer);

        bzero(buffer, MAXBUFFERLEN);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1); // Reception reponse serveur
        if (ecode == -1) {
                perror("probleme de lecture sur le serveur\n"); //STUCK AT 425
                exit(3); //Besoin d'une socket de données, d'une traduction de PORT
        }
        buffer[ecode] = '\0';
        //printf("LA4 : %s\n", buffer);

        ecode = write(connexionClientCtrl, buffer, strlen(buffer)); //Transfert reponse serveur->client
        if (ecode == -1) {
                perror("probleme de lecture sur le client");
                exit(3);
        }

        ecode = read(dataServeur, buffer, MAXBUFFERLEN - 1);
        if (ecode == -1) {
                perror("Probleme de lecture");
                exit(3);
        }
        //while() pour transférer les données du vrai serveur au vrai client sur les sockets datas
        while (ecode != 0) {
                ecode = write(dataClient, buffer, strlen(buffer));
                bzero(buffer, MAXBUFFERLEN - 1);
                ecode = read(dataServeur, buffer, MAXBUFFERLEN - 1);
                if (ecode == -1) {
                        perror("probleme de lecture");
                        exit(3);
                }
        }

        close(dataClient);
        close(dataServeur);

        bzero(buffer, MAXBUFFERLEN - 1);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1);
        if (ecode == -1) {
                perror("probleme de lecture");
                exit(3);
        }

        ecode = write(connexionClientCtrl, buffer, strlen(buffer));

        bzero(buffer, MAXBUFFERLEN);
        char cmdQuit[10] = "quit\n";
        strncat(buffer, cmdQuit, strlen(cmdQuit));
        ecode = write(connexionServeur, buffer, strlen(buffer)); //commande quit

        bzero(buffer, MAXBUFFERLEN);
        ecode = read(connexionServeur, buffer, MAXBUFFERLEN - 1); //reponse a quit 226
        if (ecode == -1) {
                perror("probleme de lecture");
                exit(3);
        }

        ecode = write(connexionClientCtrl, buffer, strlen(buffer));
        //ecriture pour reponse vers client
        //226 pour la fin de transmission après le while

        //Fermeture des connexions
        close(connexionClientCtrl);
        close(descSockRDV);
        close(connexionServeur);
}